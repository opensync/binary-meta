METADEBROOT2X = $(shell pwd)/rootdir-deb-2x
METADEBROOT = $(shell pwd)/rootdir-deb
METARPMROOT = $(shell pwd)/rootdir-rpm
METARPMROOT2X = $(shell pwd)/rootdir-rpm-2x
SNAPSHOTNAME = binary-meta-snapshot-$(shell date '+%Y%m%d%H%M')

# Do 'make USE_RPATH=off' to disable rpath in source builds
ifeq ($(USE_RPATH),off)
CMAKE_RPATH_FLAGS = -DCMAKE_SKIP_RPATH:BOOL=TRUE
CONFIGURE_RPATH_FLAGS = --enable-rpathhack
else
CMAKE_RPATH_FLAGS =
CONFIGURE_RPATH_FLAGS =
endif

# Prefix and PKG_CONFIG_PATH for the src target
PREFIXLATEST = $(shell pwd)/sources/rootdir
PCPLATEST = $(PREFIXLATEST)/lib/pkgconfig

# Prefix and PKG_CONFIG_PATH for the src-0.2x target
PREFIX2X = $(shell pwd)/sources/rootdir-0.2x
PCP2X = $(PREFIX2X)/lib/pkgconfig

# Binary build option: Change to "kdesu" to force rpm builds
# to use kdesu for Barry Desktop instead of beesu.
BARRY_GUISU = beesu

# Make flag for source builds
JOBSFLAG = -j2

# Compiler flags for source builds
# Depending on how new your system is, you may wish to add
# -Wno-deprecated-declarations here.
META_CFLAGS = -Wall -Werror -O2 -fno-strict-aliasing
META_CXXFLAGS = -Wall -Werror -O2 -fno-strict-aliasing

# Virtual submodule commits for the 0.2x source trees
OPENSYNC2X_BRANCH = 291a62adc1e5827b924f5dfcbc3b292531dc845e
EVOLUTION22X_BRANCH = c0d7c4ef4a26564f14b7b70f4ba7d629805856ec
FILESYNC2X_BRANCH = c259a8fc77997b6be93d7999f307134baafd6e8a
GOOGLECALENDAR2X_BRANCH = d6cbb7120f02afc3b7d51bce85815f35561fa1c8
MSYNCTOOL2X_BRANCH = 8dad16e107b0c5892fa92c57e1996c17b1cbf41a
KDEPIM2X_BRANCH = 698a260f962f476d888fd28d977bb41ad27a3cd2

# This is needed so sourcing the setup-env.sh script works
# on all platforms, such as Ubuntu Natty which likes dash over bash.
SHELL = /bin/bash

all: helpdefault
	@if [ -f helpdefault ] ; then $(MAKE) src ; fi

helpdefault:
	@$(MAKE) -s help

help:
	@echo "Top level targets:"
	@echo "=================="
	@echo
	@echo "   debian-latest        All latest debian packages"
	@echo "   debian-0.2x          All 0.2x based debian packages"
	@echo "   debian-all           All debian packages"
	@echo
	@echo "   rpm-latest           All latest RPM packages"
	@echo "   rpm-0.2x             All 0.2x based RPM packages"
	@echo "   rpm-all              All RPM packages"
	@echo
	@echo "Individual targets:"
	@echo "==================="
	@echo
	@echo "   deb-os-latest        Latest OpenSync debian packages"
	@echo "   deb-os-0.2x          OpenSync 0.2x debian packages"
	@echo "   deb-barry-latest     Barry with 0.4x plugin"
	@echo "   deb-barry-0.2x       Barry with 0.2x plugin"
	@echo "   deb-barry-both       Barry with 0.2x and 0.4x plugins"
	@echo "   deb-evo2-latest      Evolution2 0.4x plugin"
	@echo "   deb-evo2-0.2x        Evolution2 0.2x plugin"
	@echo "   deb-fs-latest        File-sync 0.4x plugin"
	@echo "   deb-fs-0.2x          File-sync 0.2x plugin"
	@echo "   deb-python-latest    Python-module 0.4x plugin"
	@echo "   deb-google-latest    Google-calendar 0.4x plugin"
	@echo "   deb-google-0.2x      Google-calendar 0.2x plugin"
	@echo "   deb-akonadi-latest   Akonadi 0.4x plugin"
	@echo "   deb-kdepim-0.2x      KDE PIM 0.2x plugin"
	@echo "   deb-vformat-latest   vformat 0.4x plugin"
	@echo "   deb-xmlformat-latest xmlformat 0.4x plugin"
	@echo "   deb-xslt-latest      Xsltformat 0.4x plugin"
	@echo "   deb-osynctool-latest Osynctool for 0.4x"
	@echo "   deb-msynctool-0.2x   msynctool for 0.2x"
	@echo "   deb-everything-0.2x  Meta package for 0.2x"
	@echo "   deb-everything-0.4x  Meta package for 0.4x"
	@echo "   deb-everything       Meta package for both 0.2x and 0.4x"
	@echo
	@echo "   rpm-os-latest        Latest OpenSync RPM packages"
	@echo "   rpm-os-0.2x          OpenSync 0.2x RPM packages"
	@echo "   rpm-barry-latest     Barry with 0.4x plugin"
	@echo "   rpm-barry-0.2x       Barry with 0.2x plugin"
	@echo "   rpm-barry-both       Barry with 0.2x and 0.4x plugins"
	@echo "   rpm-xmlformat-latest xmlformat 0.4x plugin"
	@echo "   rpm-vformat-latest   vformat 0.4x plugin"
	@echo "   rpm-evo2-latest      Evolution2 0.4x plugin"
	@echo "   rpm-evo2-0.2x        Evolution2 0.2x plugin"
	@echo "   rpm-fs-latest        File-sync 0.4x plugin"
	@echo "   rpm-python-latest    Python-module 0.4x plugin"
	@echo "   rpm-osynctool-latest Osynctool for 0.4x"
	@echo "   rpm-msynctool-0.2x   msynctool for 0.2x"
	@echo "   rpm-everything-0.2x  Meta package for 0.2x"
	@echo "   rpm-everything-0.4x  Meta package for 0.4x"
	@echo "   rpm-everything       Meta package for both 0.2x and 0.4x"
	@echo
	@echo "Note: you can add -build to most of the above targets in order"
	@echo "to control them manually, for example, if you have opensync"
	@echo "installed on your system already and don't need to build it here."
	@echo "  For example: make deb-evo2-latest-build"
	@echo
	@echo "Additional binary targets:"
	@echo "=========================="
	@echo
	@echo "These targets are not included in debian-all / rpm-all, since"
	@echo "the dependencies are not available on all supported platforms,"
	@echo "or not required on all platforms."
	@echo
	@echo "   deb-evo3-latest      Evolution3 0.4x plugin"
	@echo "   rpm-evo3-latest      RPM package for evolution 3 API plugin"
	@echo
	@echo "   deb-libtar           libtar (uses latest upstream release)"
	@echo "   rpm-libtar           for distros like Ubuntu 12.04 or"
	@echo "                        openSUSE"
	@echo
	@echo "Top level source targets:"
	@echo "========================="
	@echo
	@echo "   src           Changes mode into a source devel tree.  Runs"
	@echo "                 cmake as needed on all the latest sources, and"
	@echo "                 builds them.  The next time you run 'make'"
	@echo "                 it will build src instead of displaying the"
	@echo "                 help text.  Each source tree will be automatically"
	@echo "                 installed into sources/rootdir as it is built."
	@echo "                 Each tree will contain a build/ dir for building,"
	@echo "                 and can be used independently during development."
	@echo "   src-0.2x      Build 0.2x sources into sources/rootdir-0.2x"
	@echo
	@echo "Individual 0.4x (latest) source targets:"
	@echo "========================================"
	@echo
	@echo "   opensync-latest-src          osynctool-latest-src"
	@echo "   barry-latest-src             akonadi-sync-latest-src"
	@echo "   evolution2-latest-src        file-sync-latest-src"
	@echo "   google-calendar-latest-src   vformat-latest-src"
	@echo "   xmlformat-latest-src         xsltformat-latest-src"
	@echo "   evolution3-latest-src        python-module-latest-src"
	@echo
	@echo "Individual 0.2x source targets:"
	@echo "==============================="
	@echo
	@echo "   opensync-0.2x-src            msynctool-0.2x-src"
	@echo "   barry-0.2x-src               kdepim-0.2x-src"
	@echo "   google-calendar-0.2x-src     file-sync-0.2x-src"
	@echo "   evolution2-0.2x-src"
	@echo
	@echo "Misc. targets:"
	@echo "=============="
	@echo
	@echo "   help          Display the help text explicitly."
	@echo "   clean         Removes all temporary files via the git-clean"
	@echo "   tarball       Creates a git-ified tarball of entire current"
	@echo "                 tree.  This is useful if you want to bundle the"
	@echo "                 entire tree to build on another machine, without"
	@echo "                 burdening the repo.or.cz server needlessly."
	@echo "   gen-sources   Generate auxiliary source trees based on other"
	@echo "                 submodules."
	@echo "   gen-autoconf  Run after gen-sources with a -j2 or higher make"
	@echo "                 option to parallelize the autoreconf steps of"
	@echo "                 the 0.2x sources."
	@echo "   fetch         Do a git fetch in each submodule"
	@echo "   debdeps       Install Debian dependencies"
	@echo "   rpmdev        Create an RPM dev tree manually"
	@echo "   rpmclean      Delete everything found under ~/rpmbuild/SOURCES"
	@echo "                 in order to guarantee a clean build."
	@echo "   opensuse      Perform any tweaks needed when building on"
	@echo "                 openSUSE.  Must be run before any other target."
	@echo
	@if pkg-config --exists opensync-1.0 ; then echo "**** WARNING: opensync 0.2x already installed. May cause problems." ; fi
	@if pkg-config --exists libopensync1 ; then echo "**** WARNING: opensync 0.4x already installed. May cause problems." ; fi
	@if uname -m | grep 64 > /dev/null ; then echo "**** NOTE: Set LIB_SUFFIX=64 if your system uses /usr/lib64" ; fi

##############################################################################
# Top level targets

debian-latest: deb-barry-latest deb-evo2-latest deb-fs-latest \
	deb-python-latest \
	deb-google-latest deb-vformat-latest deb-xmlformat-latest \
	deb-xslt-latest deb-osynctool-latest deb-akonadi-latest \
	deb-everything-0.4x
	touch debian-latest
debian-0.2x: deb-barry-0.2x deb-evo2-0.2x deb-fs-0.2x deb-google-0.2x \
	deb-kdepim-0.2x deb-msynctool-0.2x \
	deb-everything-0.2x
	touch debian-0.2x
debian-all: deb-barry-both deb-evo2-latest deb-evo2-0.2x \
	deb-fs-latest deb-fs-0.2x \
	deb-python-latest \
	deb-google-latest deb-google-0.2x \
	deb-akonadi-latest deb-kdepim-0.2x \
	deb-vformat-latest deb-xmlformat-latest deb-xslt-latest \
	deb-osynctool-latest deb-msynctool-0.2x \
	deb-everything-0.2x deb-everything-0.4x deb-everything
	touch debian-all
rpm-latest: rpm-barry-latest rpm-xmlformat-latest rpm-vformat-latest \
	rpm-evo2-latest rpm-osynctool-latest \
	rpm-fs-latest rpm-python-latest \
	rpm-everything-0.4x
	touch rpm-latest
rpm-0.2x: rpm-barry-0.2x \
	rpm-msynctool-0.2x \
	rpm-evo2-0.2x \
	rpm-everything-0.2x
	touch rpm-0.2x
rpm-all: rpm-barry-both \
	rpm-xmlformat-latest \
	rpm-vformat-latest \
	rpm-evo2-latest \
	rpm-evo2-0.2x \
	rpm-fs-latest \
	rpm-python-latest \
	rpm-osynctool-latest \
	rpm-msynctool-0.2x \
	rpm-everything
	touch rpm-all

##############################################################################
# Individual targets
#
# These targets use sub-makes so that the -build targets are separate and
# are not burdened with the proper dependencies.  This allows the user to
# build the low level targets in any order he wants, and avoid source
# dependencies in favour of system libraries.
#

deb-os-latest: deb-os-latest-build
	touch deb-os-latest
deb-os-0.2x: deb-os-0.2x-build
	touch deb-os-0.2x
deb-barry-0.2x: deb-os-0.2x
	$(MAKE) deb-barry-build
	$(MAKE) deb-barry-0.2x-build
	touch deb-barry-0.2x
deb-barry-latest: deb-os-latest
	$(MAKE) deb-barry-build
	$(MAKE) deb-barry-0.4x-build
	touch deb-barry-latest
deb-barry-both: deb-os-0.2x-build deb-os-latest-build
	$(MAKE) deb-barry-build
	# make plugins in serial due to make install symlinks disappearing
	$(MAKE) deb-barry-0.2x-build
	$(MAKE) deb-barry-0.4x-build
	touch deb-barry-both
deb-evo2-latest: deb-os-latest
	$(MAKE) deb-evo2-latest-build
	touch deb-evo2-latest
deb-evo3-latest: deb-os-latest
	$(MAKE) deb-evo3-latest-build
	touch deb-evo3-latest
deb-evo2-0.2x: deb-os-0.2x
	$(MAKE) deb-evo2-0.2x-build
	touch deb-evo2-0.2x
deb-fs-latest: deb-os-latest
	$(MAKE) deb-fs-latest-build
	touch deb-fs-latest
deb-fs-0.2x: deb-os-0.2x
	$(MAKE) deb-fs-0.2x-build
	touch deb-fs-0.2x
deb-python-latest: deb-os-latest
	$(MAKE) deb-python-latest-build
	touch deb-python-latest
deb-google-latest: deb-os-latest
	$(MAKE) deb-google-latest-build
	touch deb-google-latest
deb-google-0.2x: deb-os-0.2x
	$(MAKE) deb-google-0.2x-build
	touch deb-google-0.2x
deb-akonadi-latest: deb-os-latest
	$(MAKE) deb-akonadi-latest-build
	touch deb-akonadi-latest
deb-kdepim-0.2x: deb-os-0.2x
	$(MAKE) deb-kdepim-0.2x-build
	touch deb-kdepim-0.2x
deb-vformat-latest: deb-os-latest
	$(MAKE) deb-vformat-latest-build
	touch deb-vformat-latest
deb-xmlformat-latest: deb-os-latest
	$(MAKE) deb-xmlformat-latest-build
	touch deb-xmlformat-latest
deb-xslt-latest: deb-os-latest
	$(MAKE) deb-xslt-latest-build
	touch deb-xslt-latest
deb-osynctool-latest: deb-os-latest
	$(MAKE) deb-osynctool-latest-build
	touch deb-osynctool-latest
deb-msynctool-0.2x: deb-os-0.2x
	$(MAKE) deb-msynctool-0.2x-build
	touch deb-msynctool-0.2x
deb-everything-0.2x: deb-everything-0.2x-build
	touch deb-everything-0.2x
deb-everything-0.4x: deb-everything-0.4x-build
	touch deb-everything-0.4x
deb-everything: deb-everything-0.2x deb-everything-0.4x deb-everything-build
	touch deb-everything

rpm-os-latest: rpm-os-latest-build
	touch rpm-os-latest
rpm-os-0.2x: rpm-os-0.2x-build
	touch rpm-os-0.2x
rpm-barry-0.2x: rpm-os-0.2x
	$(MAKE) -j1 rpm-barry-0.2x-build rpm-barry-finish
	touch rpm-barry-0.2x
rpm-msynctool-0.2x: rpm-os-0.2x
	$(MAKE) rpm-msynctool-0.2x-build
	touch rpm-msynctool-0.2x
rpm-barry-latest: rpm-os-latest
	$(MAKE) -j1 rpm-barry-0.4x-build rpm-barry-finish
	touch rpm-barry-latest
rpm-barry-both: rpm-os-0.2x rpm-os-latest
	$(MAKE) -j1 rpm-barry-both-build rpm-barry-finish
	touch rpm-barry-both
rpm-xmlformat-latest: rpm-os-latest
	$(MAKE) rpm-xmlformat-latest-build
	touch rpm-xmlformat-latest
rpm-vformat-latest: rpm-os-latest
	$(MAKE) rpm-vformat-latest-build
	touch rpm-vformat-latest
rpm-evo2-latest: rpm-os-latest
	$(MAKE) rpm-evo2-latest-build
	touch rpm-evo2-latest
rpm-evo2-0.2x: rpm-os-0.2x
	$(MAKE) rpm-evo2-0.2x-build
	touch rpm-evo2-0.2x
rpm-fs-latest: rpm-os-latest
	$(MAKE) rpm-fs-latest-build
	touch rpm-fs-latest
rpm-python-latest: rpm-os-latest
	$(MAKE) rpm-python-latest-build
	touch rpm-python-latest
rpm-osynctool-latest: rpm-os-latest
	$(MAKE) rpm-osynctool-latest-build
	touch rpm-osynctool-latest
rpm-everything-0.2x: rpm-everything-0.2x-build
	touch rpm-everything-0.2x
rpm-everything-0.4x: rpm-everything-0.4x-build
	touch rpm-everything-0.4x
rpm-everything: rpm-everything-0.2x rpm-everything-0.4x rpm-everything-build
	touch rpm-everything

##############################################################################
# Additional binary packages

rpm-evo3-latest: rpm-os-latest
	$(MAKE) rpm-evo3-latest-build
	touch rpm-evo3-latest
deb-libtar: deb-libtar-build
	touch deb-libtar
rpm-libtar: rpm-libtar-build
	touch rpm-libtar

##############################################################################
# Misc setup
#
# We use touch files for the directories, since directory timestamps can
# change as we install into them.

deb-rootdir:
	mkdir -p $(METADEBROOT)
	touch deb-rootdir

deb-rootdir2x:
	mkdir -p $(METADEBROOT2X)
	touch deb-rootdir2x

deb-rootdirs: deb-rootdir deb-rootdir2x
	touch deb-rootdirs

rpm-rootdir:
	mkdir -p $(METARPMROOT)
	touch rpm-rootdir

rpm-rootdir2x:
	mkdir -p $(METARPMROOT2X)
	touch rpm-rootdir2x

rpm-rootdirs: rpm-rootdir rpm-rootdir2x
	touch rpm-rootdirs

##############################################################################
# Gen-sources

# This is the list of source trees generated from other git submodules.
# i.e. such as older versions of the same packages
gen-sources: sources/opensync-0.2x sources/evolution2-0.2x \
	sources/file-sync-0.2x sources/google-calendar-0.2x \
	sources/msynctool-0.2x sources/kdepim-0.2x

# parallel autoconf build (see source target section for implicit details)
gen-autoconf: sources/opensync-0.2x/configure \
	sources/msynctool-0.2x/configure \
	sources/barry/configure \
	sources/evolution2-0.2x/configure \
	sources/file-sync-0.2x/configure\
	sources/google-calendar-0.2x/configure \
	sources/kdepim-0.2x/configure

sources/opensync-0.2x:
	# start fresh
	rm -rf sources/opensync-0.2x
	# space-saving clone of the opensync submodule
	git clone -s sources/opensync sources/opensync-0.2x
	# This SHA1SUM is the latest known-good branch-0.2x point
	(cd sources/opensync-0.2x && \
		git checkout $(OPENSYNC2X_BRANCH))

sources/evolution2-0.2x:
	rm -rf sources/evolution2-0.2x
	git clone -s sources/evolution2 sources/evolution2-0.2x
	(cd sources/evolution2-0.2x && \
		git checkout $(EVOLUTION22X_BRANCH))

sources/file-sync-0.2x:
	rm -rf sources/file-sync-0.2x
	git clone -s sources/file-sync sources/file-sync-0.2x
	(cd sources/file-sync-0.2x && \
		git checkout $(FILESYNC2X_BRANCH))

sources/google-calendar-0.2x:
	rm -rf sources/google-calendar-0.2x
	git clone -s sources/google-calendar sources/google-calendar-0.2x
	(cd sources/google-calendar-0.2x && \
		git checkout $(GOOGLECALENDAR2X_BRANCH))

sources/msynctool-0.2x:
	rm -rf sources/msynctool-0.2x
	git clone -s sources/osynctool sources/msynctool-0.2x
	(cd sources/msynctool-0.2x && \
		git checkout $(MSYNCTOOL2X_BRANCH))

sources/kdepim-0.2x:
	rm -rf sources/kdepim-0.2x
	git clone -s sources/kdepim sources/kdepim-0.2x
	(cd sources/kdepim-0.2x && \
		git checkout $(KDEPIM2X_BRANCH))

##############################################################################
# OpenSync 0.2x

deb-os-0.2x-build: deb-rootdirs sources/opensync-0.2x
	# don't build again if already installed
	#if pkg-config --exists opensync-1.0 ; then exit 1 ; fi
	# build binary packages
	(cd debian/opensync-0.2x && fakeroot -- debian/rules binary)
	# install into rootdir/ as build dependency for other packages
	(cd debian/opensync-0.2x && \
		debian/rules metainstall METADESTDIR=$(METADEBROOT2X))
	# remove .la files
	(cd $(METADEBROOT2X) && find . -name "*.la" -print0 |xargs -0 rm)
	# add symlink from main root to ours
	mkdir -p $(METADEBROOT)/usr/include
	ln -s $(METADEBROOT2X)/usr/include/* $(METADEBROOT)/usr/include
	# done
	touch deb-os-0.2x-build deb-os-either-build

##############################################################################
# OpenSync 0.4x
#
# The extra METADEBROOT2X is not needed here, but due to a race condition in
# make 3.81, opensync 0.2x and -latest builds do not happen in parallel
# in some cases without this.

deb-os-latest-build: deb-rootdirs
	# don't build again if already installed
	#if pkg-config --exists libopensync1 ; then exit 1 ; fi
	# build binary packages
	(cd debian/opensync-latest && fakeroot -- debian/rules binary)
	# install into rootdir/ as build dependency for other packages
	(cd debian/opensync-latest && \
		debian/rules metainstall METADESTDIR=$(METADEBROOT))
	# done
	touch deb-os-latest-build deb-os-either-build

##############################################################################
# Barry

deb-barry-build:
	# Note: This build includes the Desktop GUI, which will detect what
	# versions of opensync you have installed and build against that.
	# If you build with 0.2x or 0.4x alone, and then later build the
	# other plugin, remember that the Desktop will only be built against
	# the first one.  So if you want both, do deb-barry-both right away.
	# (See the dedicated rules above)
	#
	(. setup-env.sh deb-os-either-build deb-barry-build && \
		cd sources/barry && \
		./buildgen.sh && \
		fakeroot -- debian/rules binary)
	# install into rootdir/ as build dependency for other packages
	(cd sources/barry && $(MAKE) -j1 install DESTDIR=$(METADEBROOT))
	# move packages into debian/
	mv sources/*.deb debian
	# done
	touch deb-barry-build

deb-barry-clean:
	(cd sources/barry && \
		fakeroot -- debian/rules clean)
	-rm deb-barry-build

deb-barry-0.2x-build: deb-barry-build
	(. setup-env.sh deb-os-0.2x-build deb-barry-0.2x-build && \
		cd sources/barry && \
		fakeroot -- debian/rules os22-binary)
	# move packages into debian/
	mv sources/barry/*.deb debian
	# done
	touch deb-barry-0.2x-build

deb-barry-0.2x-clean:
	(cd sources/barry && \
		fakeroot -- debian/rules os22-clean)
	-rm deb-barry-0.2x-build

deb-barry-0.4x-build: deb-barry-build
	(. setup-env.sh deb-os-latest-build deb-barry-0.4x-build && \
		cd sources/barry && \
		fakeroot -- debian/rules os4x-binary)
	# move packages into debian/
	mv sources/barry/*.deb debian
	# done
	touch deb-barry-0.4x-build

deb-barry-0.4x-clean:
	(cd sources/barry && \
		fakeroot -- debian/rules os4x-clean)
	-rm deb-barry-0.4x-build

##############################################################################
# Evolution2 plugin

deb-evo2-0.2x-build: $(METADEBROOT2X) sources/evolution2-0.2x
	(. setup-env.sh deb-os-0.2x-build deb-evo2-0.2x-build && \
		cd debian/evolution2-0.2x && \
		fakeroot -- debian/rules binary)
	touch deb-evo2-0.2x-build

deb-evo2-latest-build:
	(. setup-env.sh deb-os-latest-build deb-evo2-latest-build && \
		cd debian/evolution2-latest && \
		fakeroot -- debian/rules binary)
	touch deb-evo2-latest-build

deb-evo2-latest-clean:
	(cd debian/evolution2-latest && \
		fakeroot -- debian/rules clean)
	-rm deb-evo2-latest-build

##############################################################################
# Evolution3 plugin

deb-evo3-latest-build:
	(. setup-env.sh deb-os-latest-build deb-evo3-latest-build && \
		cd debian/evolution3-latest && \
		fakeroot -- debian/rules binary)
	touch deb-evo3-latest-build

deb-evo3-latest-clean:
	(cd debian/evolution3-latest && \
		fakeroot -- debian/rules clean)
	-rm deb-evo3-latest-build

##############################################################################
# File-sync plugin

deb-fs-0.2x-build: $(METADEBROOT2X) sources/file-sync-0.2x
	(. setup-env.sh deb-os-0.2x-build deb-fs-0.2x-build && \
		cd debian/file-sync-0.2x && \
		fakeroot -- debian/rules binary)
	touch deb-fs-0.2x-build

deb-fs-latest-build:
	(. setup-env.sh deb-os-latest-build deb-fs-latest-build && \
		cd debian/file-sync-latest && \
		fakeroot -- debian/rules binary)
	touch deb-fs-latest-build

##############################################################################
# python-module plugin

deb-python-latest-build:
	(. setup-env.sh deb-os-latest-build deb-python-latest-build && \
		cd debian/python-module-latest && \
		fakeroot -- debian/rules binary)
	touch deb-python-latest-build

##############################################################################
# Google-calendar plugin

deb-google-0.2x-build: $(METADEBROOT2X) sources/google-calendar-0.2x
	(. setup-env.sh deb-os-0.2x-build deb-google-0.2x-build && \
		cd debian/google-calendar-0.2x && \
		fakeroot -- debian/rules binary)
	touch deb-google-0.2x-build

deb-google-latest-build:
	(. setup-env.sh deb-os-latest-build deb-google-latest-build && \
		cd debian/google-calendar-latest && \
		fakeroot -- debian/rules binary)
	touch deb-google-latest-build

##############################################################################
# Akonadi-sync plugin

deb-akonadi-latest-build:
	(. setup-env.sh deb-os-latest-build deb-akonadi-latest-build && \
		cd debian/akonadi-sync-latest && \
		fakeroot -- debian/rules binary)
	touch deb-akonadi-latest-build

##############################################################################
# KDE PIM plugin

deb-kdepim-0.2x-build: $(METADEBROOT2X) sources/kdepim-0.2x
	(. setup-env.sh deb-os-0.2x-build deb-kdepim-0.2x-build && \
		cd debian/kdepim-0.2x && \
		fakeroot -- debian/rules binary)
	touch deb-kdepim-0.2x-build

deb-kdepim-latest-build:
	(. setup-env.sh deb-os-latest-build deb-kdepim-latest-build && \
		cd debian/kdepim-latest && \
		fakeroot -- debian/rules binary)
	touch deb-kdepim-latest-build

##############################################################################
# VFormat plugin

deb-vformat-latest-build:
	(. setup-env.sh deb-os-latest-build deb-vformat-latest-build && \
		cd debian/vformat-latest && \
		fakeroot -- debian/rules binary)
	touch deb-vformat-latest-build

##############################################################################
# XMLFormat plugin

deb-xmlformat-latest-build:
	(. setup-env.sh deb-os-latest-build deb-xmlformat-latest-build && \
		cd debian/xmlformat-latest && \
		fakeroot -- debian/rules binary)
	touch deb-xmlformat-latest-build

##############################################################################
# XSLT Format plugin

deb-xslt-latest-build:
	(. setup-env.sh deb-os-latest-build deb-xslt-latest-build && \
		cd debian/xsltformat-latest && \
		fakeroot -- debian/rules binary)
	touch deb-xslt-latest-build

##############################################################################
# OSyncTool

deb-osynctool-latest-build:
	(. setup-env.sh deb-os-latest-build deb-osynctool-latest-build && \
		cd debian/osynctool-latest && \
		fakeroot -- debian/rules binary)
	touch deb-osynctool-latest-build

##############################################################################
# MSyncTool (0.2x)

deb-msynctool-0.2x-build: sources/msynctool-0.2x
	(. setup-env.sh deb-os-0.2x-build deb-msynctool-0.2x-build && \
		cd debian/msynctool-0.2x && \
		fakeroot -- debian/rules binary)
	touch deb-msynctool-0.2x-build

##############################################################################
# Meta packages

deb-everything-0.2x-build:
	(cd debian/binarymeta2x && fakeroot -- debian/rules binary)
	touch deb-everything-0.2x-build

deb-everything-0.4x-build:
	(cd debian/binarymeta4x && fakeroot -- debian/rules binary)
	touch deb-everything-0.4x-build

deb-everything-build:
	(cd debian/binarymeta-everything && fakeroot -- debian/rules binary)
	touch deb-everything-build



##############################################################################
##############################################################################
#                       RPM targets
##############################################################################
##############################################################################


##############################################################################
# Make sure that the rpm build directory exists in user's home

rpmbuilddir:
	@if ! test -d ~/rpmbuild ; then echo "The RPM build directory does not exist.  Run rpmdev-setuptree to initialize, or if your system doesn't have such a command, run 'make rpmdev' to do it manually." ; exit 1 ; else touch rpmbuilddir ; fi

rpmclean: rpmbuilddir
	rm -rf ~/rpmbuild/SOURCES/*
	touch rpmclean

##############################################################################
# OpenSync 0.4x

rpm-os-latest-build: rpmbuilddir rpm-rootdir
	# don't build again if already installed
#	if pkg-config --exists libopensync1 ; then exit 1 ; fi
	# build binary packages
	./rprepare.sh opensync rpm/opensync-latest.spec include_cmake
	(cd ~/rpmbuild/SPECS && rpmbuild -ba opensync-latest.spec)
	# install into rootdir/ as build dependency for other packages
	(cd ~/rpmbuild/BUILD/opensync/build && \
		$(MAKE) -j1 DESTDIR=$(METARPMROOT) install)
	# done
	touch rpm-os-latest-build

##############################################################################
# XMLFormat plugin

rpm-xmlformat-latest-build: rpmbuilddir rpm-rootdir
	# build binary packages
	./rprepare.sh xmlformat rpm/xmlformat-latest.spec include_cmake
	(. setup-env.sh rpm-os-latest-build rpm-xmlformat-0.4x-build && \
		cd ~/rpmbuild/SPECS && \
		rpmbuild -ba xmlformat-latest.spec)
	# install into rootdir/ as build dependency for other packages
	(cd ~/rpmbuild/BUILD/xmlformat/build && \
		$(MAKE) -j1 DESTDIR=$(METARPMROOT) install)
	# done
	touch rpm-xmlformat-latest-build

##############################################################################
# VFormat plugin

rpm-vformat-latest-build: rpmbuilddir rpm-rootdir
	# build binary packages
	./rprepare.sh vformat rpm/vformat-latest.spec include_cmake
	(. setup-env.sh rpm-os-latest-build rpm-vformat-0.4x-build && \
		cd ~/rpmbuild/SPECS && \
		rpmbuild -ba vformat-latest.spec)
	# install into rootdir/ as build dependency for other packages
	(cd ~/rpmbuild/BUILD/vformat/build && \
		$(MAKE) -j1 DESTDIR=$(METARPMROOT) install)
	# done
	touch rpm-vformat-latest-build

##############################################################################
# Evolution plugin

rpm-evo2-latest-build: rpmbuilddir rpm-rootdir
	# build binary packages
	./rprepare.sh evolution2 rpm/evolution2-latest.spec include_cmake
	(. setup-env.sh rpm-os-latest-build rpm-evo2-0.4x-build && \
		cd ~/rpmbuild/SPECS && \
		rpmbuild -ba evolution2-latest.spec)
	# install into rootdir/ as build dependency for other packages
	(cd ~/rpmbuild/BUILD/evolution2/build && \
		$(MAKE) -j1 DESTDIR=$(METARPMROOT) install)
	# done
	touch rpm-evo2-latest-build

##############################################################################
# Evolution3 plugin

rpm-evo3-latest-build: rpmbuilddir rpm-rootdir
	# build binary packages
	./rprepare.sh evolution3 rpm/evolution3-latest.spec include_cmake
	(. setup-env.sh rpm-os-latest-build rpm-evo3-0.4x-build && \
		cd ~/rpmbuild/SPECS && \
		rpmbuild -ba evolution3-latest.spec)
	# install into rootdir/ as build dependency for other packages
	(cd ~/rpmbuild/BUILD/evolution3/build && \
		$(MAKE) -j1 DESTDIR=$(METARPMROOT) install)
	# done
	touch rpm-evo3-latest-build

##############################################################################
# File-sync plugin

rpm-fs-latest-build: rpmbuilddir rpm-rootdir
	# build binary packages
	./rprepare.sh file-sync rpm/filesync-latest.spec include_cmake
	(. setup-env.sh rpm-os-latest-build rpm-filesync-0.4x-build && \
		cd ~/rpmbuild/SPECS && \
		rpmbuild -ba filesync-latest.spec)
	# install into rootdir/ as build dependency for other packages
	(cd ~/rpmbuild/BUILD/file-sync/build && \
		$(MAKE) -j1 DESTDIR=$(METARPMROOT) install)
	# done
	touch rpm-fs-latest-build

##############################################################################
# python-module plugin

rpm-python-latest-build: rpmbuilddir rpm-rootdir
	# build binary packages
	./rprepare.sh python-module rpm/pythonmodule-latest.spec include_cmake
	(. setup-env.sh rpm-os-latest-build rpm-pythonmodule-0.4x-build && \
		cd ~/rpmbuild/SPECS && \
		rpmbuild -ba pythonmodule-latest.spec)
	# install into rootdir/ as build dependency for other packages
	(cd ~/rpmbuild/BUILD/python-module/build && \
		$(MAKE) -j1 DESTDIR=$(METARPMROOT) install)
	# done
	touch rpm-python-latest-build

##############################################################################
# OsyncTool

rpm-osynctool-latest-build: rpmbuilddir rpm-rootdir
	# build binary packages
	./rprepare.sh osynctool rpm/osynctool-latest.spec include_cmake
	(. setup-env.sh rpm-os-latest-build rpm-osynctool-0.4x-build && \
		cd ~/rpmbuild/SPECS && \
		rpmbuild -ba osynctool-latest.spec)
	# install into rootdir/ as build dependency for other packages
	(cd ~/rpmbuild/BUILD/osynctool/build && \
		$(MAKE) -j1 DESTDIR=$(METARPMROOT) install)
	# done
	touch rpm-osynctool-latest-build

##############################################################################
# OpenSync 0.2x

rpm-os-0.2x-build: rpmbuilddir rpm-rootdirs sources/opensync-0.2x
	# don't build again if already installed
#	if pkg-config --exists opensync-1.0 ; then exit 1 ; fi
	# build binary packages
	./rprepare.sh opensync-0.2x rpm/opensync-0.2x.spec
	(cd ~/rpmbuild/SPECS && rpmbuild -ba opensync-0.2x.spec)
	# install into rootdir/ as build dependency for other packages
	(cd ~/rpmbuild/BUILD/opensync-0.2x && \
		$(MAKE) -j1 DESTDIR=$(METARPMROOT2X) install)
	# remove .la files
	(cd $(METARPMROOT2X) && find . -name "*.la" -print0 | xargs -0 rm)
	# add symlink from main root to ours
	mkdir -p $(METARPMROOT)/usr/include
	ln -s $(METARPMROOT2X)/usr/include/* $(METARPMROOT)/usr/include
	# done
	touch rpm-os-0.2x-build

##############################################################################
# msynctool

rpm-msynctool-0.2x-build: rpmbuilddir rpm-rootdir2x sources/msynctool-0.2x
	# build binary packages
	./rprepare.sh msynctool-0.2x rpm/msynctool-0.2x.spec
	(. setup-env.sh rpm-os-0.2x-build rpm-msynctool-0.2x-build && \
		cd ~/rpmbuild/SPECS && \
		rpmbuild -ba msynctool-0.2x.spec)
	# install into rootdir/ as build dependency for other packages
	(cd ~/rpmbuild/BUILD/msynctool-0.2x && \
		$(MAKE) -j1 DESTDIR=$(METARPMROOT2X) install)
	# done
	touch rpm-msynctool-0.2x-build

##############################################################################
# Evolution 0.2x plugin

rpm-evo2-0.2x-build: rpmbuilddir rpm-rootdir
	# build binary packages
	./rprepare.sh evolution2-0.2x rpm/evolution2-0.2x.spec
	(. setup-env.sh rpm-os-0.2x-build rpm-evo2-0.2x-build && \
		cd ~/rpmbuild/SPECS && \
		rpmbuild -ba evolution2-0.2x.spec)
	# install into rootdir/ as build dependency for other packages
	(cd ~/rpmbuild/BUILD/evolution2-0.2x && \
		$(MAKE) -j1 DESTDIR=$(METARPMROOT2X) install)
	# remove .la files
	(cd $(METARPMROOT2X) && find . -name "*.la" -print0 | xargs -0 rm)
	# done
	touch rpm-evo2-0.2x-build

##############################################################################
# Barry
#
# Note: This build includes the Desktop GUI, which will detect what
# versions of opensync you have installed and build against that.
# If you build with 0.2x or 0.4x alone, and then later build the
# other plugin, remember that the Desktop will only be built against
# the first one.  So if you want both, do deb-barry-both right away.
# (See the dedicated rules above)

rpm-barry-0.2x-build:
	./rprepare.sh barry sources/barry/rpm/barry.spec
	sed -i "s/^Source: .*$$/Source: barry.tar.bz2/" ~/rpmbuild/SPECS/barry.spec
	sed -i "s/^BuildRequires:/#BuildRequires:/" ~/rpmbuild/SPECS/barry.spec
	sed -i "/^%setup/ s/$$/ -n barry/" ~/rpmbuild/SPECS/barry.spec
	sed -i "s/%{?_smp_mflags}//" ~/rpmbuild/SPECS/barry.spec
	(. setup-env.sh rpm-os-0.2x-build rpm-barry-0.2x-build && \
		cd ~/rpmbuild/SPECS && \
		if [ "${BARRY_GUISU}" = "kdesu" ] ; then export GUISU_OPTION="--with kdesu" ; fi && \
		rpmbuild -ba barry.spec \
			--with gui \
			--with opensync \
			--with desktop \
			$$GUISU_OPTION)
	touch rpm-barry-0.2x-build

rpm-barry-0.4x-build:
	./rprepare.sh barry sources/barry/rpm/barry.spec
	sed -i "s/^Source: .*$$/Source: barry.tar.bz2/" ~/rpmbuild/SPECS/barry.spec
	sed -i "s/^BuildRequires:/#BuildRequires:/" ~/rpmbuild/SPECS/barry.spec
	sed -i "/^%setup/ s/$$/ -n barry/" ~/rpmbuild/SPECS/barry.spec
	sed -i "s/%{?_smp_mflags}//" ~/rpmbuild/SPECS/barry.spec
	(. setup-env.sh rpm-os-latest-build rpm-barry-0.4x-build && \
		cd ~/rpmbuild/SPECS && \
		if [ "${BARRY_GUISU}" = "kdesu" ] ; then export GUISU_OPTION="--with kdesu" ; fi && \
		rpmbuild -ba barry.spec \
			--with gui \
			--with opensync4x \
			--with desktop \
			$$GUISU_OPTION)
	touch rpm-barry-0.4x-build

rpm-barry-both-build:
	./rprepare.sh barry sources/barry/rpm/barry.spec
	sed -i "s/^Source: .*$$/Source: barry.tar.bz2/" ~/rpmbuild/SPECS/barry.spec
	sed -i "s/^BuildRequires:/#BuildRequires:/" ~/rpmbuild/SPECS/barry.spec
	sed -i "/^%setup/ s/$$/ -n barry/" ~/rpmbuild/SPECS/barry.spec
	sed -i "s/%{?_smp_mflags}//" ~/rpmbuild/SPECS/barry.spec
	(. setup-env.sh rpm-os-latest-build rpm-barry-both-build && \
		cd ~/rpmbuild/SPECS && \
		if [ "${BARRY_GUISU}" = "kdesu" ] ; then export GUISU_OPTION="--with kdesu" ; fi && \
		rpmbuild -ba barry.spec \
			--with gui \
			--with opensync \
			--with opensync4x \
			--with desktop \
			$$GUISU_OPTION)
	touch rpm-barry-both-build

rpm-barry-finish:
	# install into rootdir/ as build dependency for other packages
	(cd ~/rpmbuild/BUILD/barry && \
		$(MAKE) -j1 DESTDIR=$(METARPMROOT) install)
	# done
	touch rpm-barry-finish

##############################################################################
# Meta packages

rpm-everything-0.2x-build:
	cp rpm/binarymeta2x.spec ~/rpmbuild/SPECS
	(cd ~/rpmbuild/SPECS && rpmbuild -bb binarymeta2x.spec)
	touch rpm-everything-0.2x-build

rpm-everything-0.4x-build:
	cp rpm/binarymeta4x.spec ~/rpmbuild/SPECS
	(cd ~/rpmbuild/SPECS && rpmbuild -bb binarymeta4x.spec)
	touch rpm-everything-0.4x-build

rpm-everything-build:
	cp rpm/binarymeta-everything.spec ~/rpmbuild/SPECS
	(cd ~/rpmbuild/SPECS && rpmbuild -bb binarymeta-everything.spec)
	touch rpm-everything-build

##############################################################################
# Misc dependencies

deb-libtar-build:
	(cd debian/libtar-latest && fakeroot -- debian/rules binary)
	touch deb-libtar-build

rpm-libtar-build: rpmbuilddir
	./rprepare.sh libtar rpm/libtar.spec
	cp rpm/libtar-rpmlintrc ~/rpmbuild/SOURCES
	(cd ~/rpmbuild/SPECS && rpmbuild -ba libtar.spec)
	touch rpm-libtar-build


##############################################################################
##############################################################################
#                       SRC targets
##############################################################################
##############################################################################

src: disable-help opensync-latest-src \
	osynctool-latest-src \
	barry-latest-src \
	akonadi-sync-latest-src \
	evolution2-latest-src \
	evolution3-latest-src \
	file-sync-latest-src \
	python-module-latest-src \
	google-calendar-latest-src \
	vformat-latest-src \
	xmlformat-latest-src \
	xsltformat-latest-src

disable-help:
	# disable default help message
	touch helpdefault

# dependency rules
opensync-latest-src: opensync-latest-src-build
osynctool-latest-src: opensync-latest-src osynctool-latest-src-build
barry-latest-src: opensync-latest-src barry-latest-src-build
akonadi-sync-latest-src: opensync-latest-src akonadi-sync-latest-src-build
evolution2-latest-src: opensync-latest-src evolution2-latest-src-build
evolution3-latest-src: opensync-latest-src evolution3-latest-src-build
file-sync-latest-src: opensync-latest-src file-sync-latest-src-build
python-module-latest-src: opensync-latest-src python-module-latest-src-build
google-calendar-latest-src: opensync-latest-src google-calendar-latest-src-build
vformat-latest-src: opensync-latest-src vformat-latest-src-build
xmlformat-latest-src: opensync-latest-src xmlformat-latest-src-build
xsltformat-latest-src: opensync-latest-src xsltformat-latest-src-build

# implicit src rules
sources/%/build/Makefile:
	(mkdir -p $(@D) && cd $(@D) && \
		PKG_CONFIG_PATH="$(PCPLATEST):$$PKG_CONFIG_PATH" \
		cmake -Wdev \
		-DCMAKE_MODULE_PATH="$(shell pwd)/sources/cmake-modules" \
		-DCMAKE_INSTALL_PREFIX="$(PREFIXLATEST)" \
		-DCMAKE_ETC_INSTALL_PREFIX="$(PREFIXLATEST)" \
		-DCMAKE_C_FLAGS:STRING="$(META_CFLAGS)" \
		-DCMAKE_CXX_FLAGS:STRING="$(META_CXXFLAGS)" \
		-DADDITIONAL_SWIG_CFLAGS:STRING="-Wno-error" \
		-DCMAKE_C_FLAGS_RELWITHDEBINFO:STRING="-g" \
		-DCMAKE_VERBOSE_MAKEFILE:BOOL=TRUE \
		$(CMAKE_RPATH_FLAGS) \
		.. )
%-latest-src-build: sources/%/build/Makefile
	(cd $(<D) && $(MAKE) $(JOBSFLAG) && $(MAKE) -j1 install)

# Barry is special... could do the library and plugin in two steps, but
# leave it like this for now.
#
# Note that the sources/barry/configure target is reused by src-0.2x below.
sources/barry/configure:
	(cd sources/barry && ./buildgen.sh)
sources/barry/build/Makefile: sources/barry/configure
	(cd sources/barry && mkdir -p build && cd build && \
		export CXXFLAGS="${CXXFLAGS} ${META_CXXFLAGS}" && \
		PKG_CONFIG_PATH="$(PCPLATEST):$$PKG_CONFIG_PATH" \
		../configure \
			--prefix="$(PREFIXLATEST)" \
			$(CONFIGURE_RPATH_FLAGS) \
			--enable-opensync-plugin-4x \
			--enable-gui \
			--enable-desktop)
barry-latest-src-build: sources/barry/build/Makefile
	(cd sources/barry/build && $(MAKE) $(JOBSFLAG) && $(MAKE) -j1 install)


##############################################################################
# 0.2x sources - caution, because the gen-sources do NOT stick around!

src-0.2x: opensync-0.2x-src \
	msynctool-0.2x-src \
	barry-0.2x-src \
	evolution2-0.2x-src \
	file-sync-0.2x-src \
	google-calendar-0.2x-src \
	kdepim-0.2x-src

# dependency rules
opensync-0.2x-src: sources/opensync-0.2x opensync-0.2x-src-build
msynctool-0.2x-src: opensync-0.2x-src sources/msynctool-0.2x \
	msynctool-0.2x-src-build
barry-0.2x-src: opensync-0.2x-src barry-0.2x-src-build
evolution2-0.2x-src: opensync-0.2x-src sources/evolution2-0.2x \
	evolution2-0.2x-src-build
google-calendar-0.2x-src: opensync-0.2x-src sources/google-calendar-0.2x \
	google-calendar-0.2x-src-build
file-sync-0.2x-src: opensync-0.2x-src sources/file-sync-0.2x \
	file-sync-0.2x-src-build
kdepim-0.2x-src: opensync-0.2x-src sources/kdepim-0.2x kdepim-0.2x-src-build

# implicit src-0.2x rules
sources/%-0.2x/configure:
	(cd $(@D) && autoreconf -if)
sources/%-0.2x/build2x/Makefile: sources/%-0.2x/configure
	(mkdir -p $(@D) && cd $(@D) && \
		export CXXFLAGS="${CXXFLAGS} ${META_CXXFLAGS}" && \
		PKG_CONFIG_PATH="$(PCP2X):$$PKG_CONFIG_PATH" ../configure \
			--prefix="$(PREFIX2X)" $(CONFIGURE_RPATH_FLAGS))
%-0.2x-src-build: sources/%-0.2x/build2x/Makefile
	(cd $(<D) && $(MAKE) $(JOBSFLAG) && $(MAKE) -j1 install)

# opensync main lib is special, to --enable-tracing
sources/opensync-0.2x/build2x/Makefile: sources/opensync-0.2x/configure
	(cd sources/opensync-0.2x && mkdir -p build2x && cd build2x && \
		export CXXFLAGS="${CXXFLAGS} ${META_CXXFLAGS}" && \
		../configure --prefix="$(PREFIX2X)" \
			--enable-tracing \
			$(CONFIGURE_RPATH_FLAGS) \
			--enable-unit-tests)

# Barry is special, using different build2x directory
sources/barry/build2x/opensync-plugin/Makefile: sources/barry/configure
	(cd sources/barry && mkdir -p build2x && cd build2x && \
		export CXXFLAGS="${CXXFLAGS} ${META_CXXFLAGS}" && \
		PKG_CONFIG_PATH="$(PCP2X):$$PKG_CONFIG_PATH" ../configure \
			--prefix="$(PREFIX2X)" \
			$(CONFIGURE_RPATH_FLAGS) \
			--enable-opensync-plugin \
			--enable-gui \
			--enable-desktop)
barry-0.2x-src-build: sources/barry/build2x/opensync-plugin/Makefile
	(cd sources/barry/build2x && $(MAKE) $(JOBSFLAG) && $(MAKE) -j1 install)



##############################################################################
##############################################################################
#                       Misc. targets
##############################################################################
##############################################################################

##############################################################################
# Tarball

tarball: clean gen-sources
	#######################################################################
	# Special Barry trigger for Ubuntu 10.04 LTS, which has gettext 0.17...
	# We need gettext 0.18.x for the Desktop build, so pre-generate it here.
	(cd sources/barry && ./buildgen.sh)
	# End of Special Barry trigger
	#######################################################################
	rm -rf /tmp/$(SNAPSHOTNAME) /tmp/$(SNAPSHOTNAME).tar.bz2
	mkdir /tmp/$(SNAPSHOTNAME)
	tar --exclude=.git -cf - . | tar -C /tmp/$(SNAPSHOTNAME) -xf -
	(cd /tmp/$(SNAPSHOTNAME) && \
		rm -f .gitmodules && \
		git init && \
		git add -f . && \
		git commit -m "Binary Meta Snapshot" && \
		git gc)
	(cd /tmp && tar -cjf $(SNAPSHOTNAME).tar.bz2 $(SNAPSHOTNAME))


##############################################################################
# Cleanup

clean:
	@# clean submodule trees that contain build cruft of their own
	git submodule foreach git clean -xdf
	@# use git to clean up the rest (the extra -f removes any generated
	@# git trees as well)
	git clean -xdff

##############################################################################
# Fetch

fetch:
	git submodule init
	git submodule update
	git submodule foreach "git fetch || (echo 'Trying again...' ; git fetch) || (echo 'Trying once more...' ; git fetch)"

##############################################################################
# Debian Dependencies (debdeps)

debdeps:
	(cd depscripts && sudo ./ubuntu-natty.sh)

##############################################################################
# openSUSE tweaks

opensuse:
	( \
	sed -i 's|libsqlite3x-devel|sqlite3-devel|' rpm/opensync-0.2x.spec && \
	sed -i 's|libsqlite3x-devel|sqlite3-devel|' rpm/opensync-latest.spec \
	)
	touch opensuse

##############################################################################
# RPM Dev tree, manually

rpmdev:
	mkdir -p ~/rpmbuild/{RPMS,SRPMS,SPECS,BUILD,BUILDROOT,SOURCES}
	echo '%_topdir      %(echo $$HOME)/rpmbuild' >> ~/.rpmmacros

# This make sure that all variables in this file are exported to the
# environment for each command.
.EXPORT_ALL_VARIABLES:

# This avoids make deleting intermediate files in our implicit rules,
# such as ...configure and ...Makefile, which are needed in later builds.
.SECONDARY:

