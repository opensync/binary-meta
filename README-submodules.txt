To add a new submodule:

	git submodule add git://somerepo/something sources/directory
	git commit

To register the submodule as part of the super tree:

	git add sources/directory
	git commit

NOTE: DO NOT USE git add directory/ SINCE THE TRAILING SLASH WILL UNDO
	THE SUBMODULE.

To update the submodule, periodically enter the directory and do:

	cd sources/directory
	git pull

	<make sure everything is correct>

	cd -
	git add sources/directory
	git commit

