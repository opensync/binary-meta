#!/usr/bin/make -f
# Based on the multi2 sample debian/rules file:
# ---
# Sample debian/rules that uses debhelper.
# This file is public domain software, originally written by Joey Hess.

#export DH_VERBOSE=1

DEB_BUILDDIR = $(CURDIR)/debian/build
DEB_SRCDIR = $(shell pwd)/../../sources/xsltformat
DEB_MODDIR = $(shell pwd)/../../sources/cmake-modules

DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)

CFLAGS = -Wall -g
ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0
else
	CFLAGS += -O2
endif

export CFLAGS

SRC	:= $(DEB_SRCDIR)
BUILD	:= $(DEB_BUILDDIR)
TARGET	:= $(CURDIR)/debian/tmp

configure: configure-stamp
configure-stamp:
	dh_testdir

	[ -d $(BUILD) ] || mkdir $(BUILD)
	cd $(BUILD) && cmake $(DEB_SRCDIR) \
		-DCMAKE_MODULE_PATH="$(DEB_MODDIR)" \
		-DCMAKE_INSTALL_PREFIX="/usr" \
		-DCMAKE_C_FLAGS:STRING="$(CFLAGS)" \
		-DCMAKE_CXX_FLAGS:STRING="$(CFLAGS)" \
		-DCMAKE_C_FLAGS_RELWITHDEBINFO:STRING="-g" \
		-DOPENSYNC_UNITTESTS=OFF \
		-DCMAKE_SKIP_RPATH:BOOL=TRUE \
		-DCMAKE_VERBOSE_MAKEFILE:BOOL=TRUE
	touch configure-stamp

build: configure build-stamp
build-stamp: 
	dh_testdir

	$(MAKE) -C $(BUILD)
	-$(MAKE) -C $(BUILD) check
	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f configure-stamp build-stamp
	rm -rf $(BUILD)
	dh_clean

install: 
install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs

	$(MAKE) -C $(BUILD) install DESTDIR=$(TARGET)
	dh_install --sourcedir=$(TARGET) --fail-missing

binary-arch: build install
	dh_testdir
	dh_testroot
	dh_installchangelogs
	dh_installdocs
	dh_installexamples
	dh_installmenu
	dh_strip 
	dh_link
	dh_compress
	dh_fixperms
	dh_makeshlibs -Xformats  ### -V ?
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary-indep: build install

binary: binary-indep binary-arch
.PHONY: configure build clean binary-indep binary-arch binary-common binary install
