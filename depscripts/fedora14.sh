#!/bin/bash

pushd "$(dirname "$0")"

yum install \
	rpmdevtools \
	rpm-build \
	ccache \
	cmake \
	pkgconfig \
	swig \
	check \
	glib2-devel \
	libsqlite3x-devel \
	libxml2-devel \
	libxslt-devel \
	python-devel \
	libgcal-devel \
	evolution-data-server-devel \
	python3-httplib2 \
	kdepimlibs-devel \
	kdelibs-devel \
	akonadi-devel

source ../sources/barry/maintainer/depscripts/fedora14.sh

popd

#	libebook1.2-dev \
#	libedata-book1.2-dev \
#	libedata-cal1.2-dev \
#	libedataserver1.2-dev \
#	python-4suite-xml \
#

