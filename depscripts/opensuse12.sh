#!/bin/bash

pushd "$(dirname "$0")"

zypper install \
	ccache \
	cmake \
	swig \
	check \
	glib2-devel \
	sqlite3-devel \
	libxml2-devel \
	libxslt-devel \
	python-devel \
	libgcal-devel \
	evolution-data-server-devel \
	python-httplib2 \
	libkdepimlibs4-devel \
	libakonadiprotocolinternals-devel

source ../sources/barry/maintainer/depscripts/opensuse12.sh

popd

