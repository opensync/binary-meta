#!/bin/bash

pushd "$(dirname "$0")"

apt-get install \
	ccache \
	cmake \
	pkg-config \
	debhelper \
	fakeroot \
	swig \
	check \
	quilt \
	libglib2.0-dev \
	libsqlite3-dev \
	libxml2-dev \
	libxslt1-dev \
	python-all-dev \
	python-support \
	libgcal-dev \
	evolution-data-server-dev \
	libebook1.2-dev \
	libedata-book1.2-dev \
	libedata-cal1.2-dev \
	libedataserver1.2-dev \
	libfam-dev \
	python-httplib2 python-4suite-xml \
	kdepimlibs5-dev kdelibs5-dev \
	libakonadi-dev

source ../sources/barry/maintainer/depscripts/ubuntu-natty.sh

popd

