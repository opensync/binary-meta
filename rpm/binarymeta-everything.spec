%dump

Name:           binarymeta-everything
Epoch:          1
Version:        1.0
Release:        1cdf
Summary:        Meta package to include all Barry and OpenSync 0.2x and 0.4x packages
Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://netdirect.ca/barry
BuildRoot:      %{_tmppath}/binarymeta-everything
Requires:       binarymeta2x binarymeta4x

%description
Meta package to include all Barry and OpenSync 0.2x and 0.4x packages

%prep

%files

%changelog
* Thu Oct 20 2011 Chris Frey <cdfrey@foursquare.net> 1.0-1cdf
- meta package

