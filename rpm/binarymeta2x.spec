%dump

Name:           binarymeta2x
Epoch:          1
Version:        1.0
Release:        1cdf
Summary:        Meta package to include all Barry and OpenSync 0.2x packages
Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://netdirect.ca/barry
BuildRoot:      %{_tmppath}/binarymeta2x
Requires:       libopensync libbarry0 barry-util barry-opensync barry-gui barry-desktop msynctool opensync0-plugin-evolution

%description
Meta package to include all Barry and OpenSync 0.2x packages

%prep

%files

%changelog
* Thu Oct 20 2011 Chris Frey <cdfrey@foursquare.net> 1.0-1cdf
- meta package

