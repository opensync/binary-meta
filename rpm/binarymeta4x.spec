%dump

Name:           binarymeta4x
Epoch:          1
Version:        1.0
Release:        2cdf
Summary:        Meta package to include all Barry and OpenSync 0.4x packages
Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://netdirect.ca/barry
BuildRoot:      %{_tmppath}/binarymeta4x
Requires:       libopensync1 osynctool libbarry0 barry-util barry-opensync4x barry-gui barry-desktop opensync1-format-vformat opensync1-format-xmlformat opensync1-plugin-file opensync1-plugin-python

%description
Meta package to include all Barry and OpenSync 0.4x packages

%prep

%files

%changelog
* Tue May 29 2012 Chris Frey <cdfrey@foursquare.net> 1.0-2cdf
- added opensync1-plugin-file

* Thu Oct 20 2011 Chris Frey <cdfrey@foursquare.net> 1.0-1cdf
- meta package

