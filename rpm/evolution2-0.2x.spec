%dump

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
Name:           opensync0-plugin-evolution
Epoch:          1
Version:        0.23
Release:        1cdf
Summary:        OpenSync 0.2x plugin for syncing with Evolution

Group:          Productivity/Other
License:        GPL v2 or later
URL:            http://www.opensync.org/
Source0:        evolution2-0.2x.tar.bz2
BuildRoot:      %{_tmppath}/evolution2-0.2x
Requires:       libopensync

%description
Evolution plugin for the OpenSync framework.
OpenSync is a synchronization framework that is platform and distribution
independent. It consists of several plugins that can be used to connect to
devices, a powerful sync-engine and the framework itself. The synchronization
framework is kept very flexible and is capable of synchronizing any type of
data, including contacts, calendar, tasks, notes and files.

%prep
%setup -q -n evolution2-0.2x

# setup the environment if there are additions (for binary-meta)
export ORIG_PKG_CONFIG_PATH="$PKG_CONFIG_PATH"
if [ -n "$ADD_TO_PKG_CONFIG_PATH" ] ; then
	export PKG_CONFIG_PATH="$ADD_TO_PKG_CONFIG_PATH:$ORIG_PKG_CONFIG_PATH"
fi
set

autoreconf -if
#%if "%{?_lib}" == "lib64"
#	-DLIB_SUFFIX=64 \
#%endif
%configure --enable-rpathhack

%build
make

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name *.la -exec rm -f {} \;

%clean
rm -rf $RPM_BUILD_ROOT

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_libdir}/opensync/plugins/evo2_sync.so
%{_datadir}/opensync/defaults/evo2-sync

%changelog
* Fri Dec  9 2011 Chris Frey <cdfrey@foursquare.net> 0.23-1
- initial spec file

