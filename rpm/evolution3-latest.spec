%dump

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
Name:           opensync1-plugin-evolution3
Epoch:          1
Version:        0.39
Release:        2cdf
Summary:        Evolution 3 Synchronization Plug-In for OpenSync

Group:          Productivity/Other
License:        LGPL v2.1 or later
URL:            http://www.opensync.org/
Source0:        evolution3.tar.bz2
BuildRoot:      %{_tmppath}/evolution3-latest
Requires:       libopensync1

%description
OpenSync is a synchronization framework that is platform and distribution
independent. It consists of several plugins that can be used to connect to
devices, a powerful sync-engine and the framework itself. The synchronization
framework is kept very flexible and is capable of synchronizing any type of
data, including contacts, calendar, tasks, notes and files.

%prep
%setup -q -n evolution3

# setup the environment if there are additions (for binary-meta)
export ORIG_PKG_CONFIG_PATH="$PKG_CONFIG_PATH"
if [ -n "$ADD_TO_PKG_CONFIG_PATH" ] ; then
	export PKG_CONFIG_PATH="$ADD_TO_PKG_CONFIG_PATH:$ORIG_PKG_CONFIG_PATH"
fi
set

mkdir build
cd build
cmake $RPM_BUILD_DIR/evolution3 \
	-DCMAKE_MODULE_PATH="$RPM_BUILD_DIR/evolution3/binary-meta-bits/cmake-modules" \
	-DCMAKE_INSTALL_PREFIX="/usr" \
	-DCMAKE_C_FLAGS_RELWITHDEBINFO:STRING="-g" \
	-DOPENSYNC_UNITTESTS=OFF \
	-DCMAKE_SKIP_RPATH:BOOL=TRUE \
%if "%{?_lib}" == "lib64"
	-DLIB_SUFFIX=64 \
%endif
	-DCMAKE_VERBOSE_MAKEFILE:BOOL=TRUE

%build
cd build
make

%install
rm -rf $RPM_BUILD_ROOT
cd build
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%{_libdir}/libopensync1/plugins/evo3-sync.so
%{_libdir}/libopensync1/formats/evo3-format.so
%{_datadir}/libopensync1/defaults/evo3-sync

%changelog
* Mon Jan  2 2012 Chris Frey <cdfrey@foursquare.net> 0.39-1
- Evolution 3 API plugin, based on evo2 plugin spec

