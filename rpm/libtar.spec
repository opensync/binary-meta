#
# spec file for package libtar
#
# This file and all modifications and additions to the pristine
# package are under the same license as the package itself.
#

# norootforbuild

Summary:        Tar file manipulation API
Name:           libtar
Version:        1.2.20
Release:        1
License:        MIT
Group:          System Environment/Libraries
URL:            http://www.feep.net/libtar/
Source0:        libtar.tar.bz2
Source1:	%{name}-rpmlintrc
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-buildroot
#BuildRequires:  zlib-devel libtool >= 2.2.6

%description
libtar is a C library for manipulating tar archives. It supports both
the strict POSIX tar format and many of the commonly-used GNU
extensions.


%package        devel
Summary:        Development files for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains libraries and header files for
developing applications that use %{name}.


%prep
%setup -q -n libtar
autoreconf -if
# set correct version for .so build
%define ltversion %(echo %{version} | tr '.' ':')
sed -i 's/-rpath $(libdir)/-rpath $(libdir) -version-number %{ltversion}/' \
  lib/Makefile.in


%build
%configure --disable-static
# Don't use rpath!
cp /usr/bin/libtool .
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make %{?_smp_mflags}

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT
# Without this we get no debuginfo and stripping
chmod +x $RPM_BUILD_ROOT%{_libdir}/libtar.so.%{version}
rm $RPM_BUILD_ROOT%{_libdir}/*.la
rm $RPM_BUILD_ROOT%{_libdir}/*.a


%clean
#rm -rf $RPM_BUILD_ROOT


%post -p /sbin/ldconfig
%postun -p /sbin/ldconfig


%files
%defattr(-,root,root,-)
%doc COPYRIGHT TODO README ChangeLog*
%{_bindir}/%{name}
%{_libdir}/lib*.so.*

%files devel
%defattr(-,root,root,-)
%{_includedir}/libtar.h
%{_includedir}/libtar_listhash.h
%{_libdir}/lib*.so
%{_mandir}/man3/*.3*


%changelog
* Wed Oct  9 2013 Chris Frey <cdfrey@foursquare.net> 1.2.20-1
- New upstream version

* Tue Dec 11 2012 Chris Frey <cdfrey@foursquare.net> 1.2.19-1
- New upstream version

* Thu Aug  2 2012 Chris Frey <cdfrey@foursquare.net> 1.2.18-1
- New upstream version

* Thu May 17 2012 Chris Frey <cdfrey@foursquare.net> 1.2.16-1
- New upstream version

* Mon Nov 21 2011 Chris Frey <cdfrey@foursquare.net> 1.2.13-1
- For version 1.2.13, based on openSUSE build service spec file

* Sat Dec 13 2008 Malcolm Lewis <coyoteuser@gmail.com> 1.2.11-18
- Hacked for libtool => 2.2.6
- Initial build for openSUSE build service

* Wed Jul 30 2008 Chris Frey <cdfrey@foursquare.net> 1.2.11-10
- Hacked up for OpenSUSE Build Service

* Mon Aug 13 2007 Hans de Goede <j.w.r.degoede@hhs.nl> 1.2.11-9
- Update License tag for new Licensing Guidelines compliance

* Mon Aug 28 2006 Hans de Goede <j.w.r.degoede@hhs.nl> 1.2.11-8
- FE6 Rebuild

* Sun Jul 23 2006 Hans de Goede <j.w.r.degoede@hhs.nl> 1.2.11-7
- Taking over as maintainer since Anvil has other priorities
- Add a bunch of patches from Debian, which build a .so instead of a .a
  and fix a bunch of memory leaks.
- Reinstate a proper devel package as we now build a .so

* Thu Mar 16 2006 Dams <anvil[AT]livna.org> - 1.2.11-6.fc5
- Modified URL and added one in Source0

* Sun May 22 2005 Jeremy Katz <katzj@redhat.com> - 1.2.11-5
- rebuild on all arches

* Fri Apr  7 2005 Michael Schwendt <mschwendt[AT]users.sf.net>
- rebuilt

* Sat Aug 16 2003 Dams <anvil[AT]livna.org> 0:1.2.11-0.fdr.3
- Merged devel and main packages
- Package provide now libtar-devel

* Tue Jul  8 2003 Dams <anvil[AT]livna.org>
- Initial build.

  
