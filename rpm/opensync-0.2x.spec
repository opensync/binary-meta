%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
Name:           libopensync
Epoch:          1
Version:        0.23
Release:        1cdf
Summary:        A synchronization framework

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.opensync.org/
Source0:        opensync-0.2x.tar.bz2
BuildRoot:      %{_tmppath}/opensync-0.2x

%description
OpenSync is a synchronization framework that is platform and distribution
independent. It consists of several plugins that can be used to connect to
devices, a powerful sync-engine and the framework itself. The synchronization
framework is kept very flexible and is capable of synchronizing any type of
data, including contacts, calendar, tasks, notes and files.

%package devel
Summary:        Development package for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{epoch}:%{version}-%{release}
Requires:       pkgconfig
Requires:       glib2-devel
Requires:       libxml2-devel
Requires:       libsqlite3x-devel

%description    devel
The %{name}-devel package contains the files needed for development
with %{name}.

%prep
%setup -q -n opensync-0.2x

# and rebuild the configure script:
autoreconf -if

%build
%configure --disable-static
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
make

#doxygen Doxyfile

%install
rm -rf $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT

find $RPM_BUILD_ROOT -type f -name *.la -exec rm -f {} \;
find $RPM_BUILD_ROOT -type f -name *.a -exec rm -f {} \;

mkdir -p $RPM_BUILD_ROOT/%{_datadir}/opensync
mkdir -p $RPM_BUILD_ROOT/%{_libdir}/opensync/formats
mkdir -p $RPM_BUILD_ROOT/%{_libdir}/opensync/{python-,}plugins

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README NEWS TODO
%{_bindir}/*
%{_libdir}/*.so.*
%{_libexecdir}/osplugin
%dir %{_libdir}/opensync
%dir %{_libdir}/opensync/formats
%dir %{_libdir}/opensync/python-plugins
%dir %{_libdir}/opensync/plugins
%{_libdir}/opensync/formats/*
%dir %{_datadir}/opensync
%{python_sitearch}/*

%files devel
%defattr(-,root,root,-)
%doc docs/*
%dir %{_includedir}/opensync-1.0
%{_includedir}/opensync-1.0/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%changelog
* Fri Oct 21 2011 Chris Frey <cdfrey@foursquare.net> 0.23-1cdf
- starting fresh rpm spec, based on Fedora's latest 0.22 spec
- bumped version to 0.23

