%dump

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
Name:           libopensync1
Epoch:          1
Version:        0.39.2
Release:        1cdf
Summary:        A synchronization framework

Group:          System Environment/Libraries
License:        LGPLv2+
URL:            http://www.opensync.org/
Source0:        opensync.tar.bz2
BuildRoot:      %{_tmppath}/opensync1
Requires:       opensync1-format-vformat opensync1-format-xmlformat opensync1-plugin-file

%description
OpenSync is a synchronization framework that is platform and distribution
independent. It consists of several plugins that can be used to connect to
devices, a powerful sync-engine and the framework itself. The synchronization
framework is kept very flexible and is capable of synchronizing any type of
data, including contacts, calendar, tasks, notes and files.

%package devel
Summary:        Development package for %{name}
Group:          Development/Libraries
Requires:       %{name} = %{epoch}:%{version}-%{release}
Requires:       pkgconfig
Requires:       glib2-devel
Requires:       libxml2-devel
Requires:       libsqlite3x-devel

%description    devel
The %{name}-devel package contains the files needed for development
with %{name}.

%prep
%setup -q -n opensync

mkdir build
cd build
cmake $RPM_BUILD_DIR/opensync \
	-DCMAKE_MODULE_PATH="$RPM_BUILD_DIR/opensync/binary-meta-bits/cmake-modules" \
	-DCMAKE_INSTALL_PREFIX="/usr" \
	-DCMAKE_C_FLAGS_RELWITHDEBINFO:STRING="-g" \
	-DOPENSYNC_UNITTESTS=OFF \
	-DCMAKE_SKIP_RPATH:BOOL=TRUE \
%if "%{?_lib}" == "lib64"
	-DLIB_SUFFIX=64 \
%endif
	-DCMAKE_VERBOSE_MAKEFILE:BOOL=TRUE

%build
cd build
make

%install
rm -rf $RPM_BUILD_ROOT
cd build
make install DESTDIR=$RPM_BUILD_ROOT

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README README-GIT
%{_bindir}/*
%{_libdir}/*.so.*
%{_libdir}/libopensync1/osplugin
%{_datadir}/libopensync1/*
%{python_sitearch}/*

%files devel
%defattr(-,root,root,-)
%doc docs/*
%dir %{_includedir}/libopensync1
%{_includedir}/libopensync1/*
%{_libdir}/*.so
%{_libdir}/pkgconfig/*

%changelog
* Thu May 17 2012 Chris Frey <cdfrey@foursquare.net> 0.39.2-1
- Upstream fixed potential null pointer access

* Tue Jun 28 2011 Chris Frey <cdfrey@foursquare.net> 0.39-1
- starting fresh rpm spec, based on Fedora's latest 0.22 spec

