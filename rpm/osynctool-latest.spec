%dump

%{!?python_sitearch: %define python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib(1)")}
Name:           osynctool
Epoch:          1
Version:        0.39
Release:        1cdf
Summary:        CLI for synchronization with OpenSync

Group:          Productivity/Other
License:        GPL v2 or later
URL:            http://www.opensync.org/
Source0:        osynctool.tar.bz2
BuildRoot:      %{_tmppath}/osynctool-latest
Requires:       libopensync1

%description
OpenSync is a synchronization framework that is platform and distribution
independent. It consists of several plugins that can be used to connect to
devices, a powerful sync-engine and the framework itself. The synchronization
framework is kept very flexible and is capable of synchronizing any type of
data, including contacts, calendar, tasks, notes and files.

%prep
%setup -q -n osynctool

# setup the environment if there are additions (for binary-meta)
export ORIG_PKG_CONFIG_PATH="$PKG_CONFIG_PATH"
if [ -n "$ADD_TO_PKG_CONFIG_PATH" ] ; then
	export PKG_CONFIG_PATH="$ADD_TO_PKG_CONFIG_PATH:$ORIG_PKG_CONFIG_PATH"
fi
set

mkdir build
cd build
cmake $RPM_BUILD_DIR/osynctool \
	-DCMAKE_MODULE_PATH="$RPM_BUILD_DIR/osynctool/binary-meta-bits/cmake-modules" \
	-DCMAKE_INSTALL_PREFIX="/usr" \
	-DCMAKE_C_FLAGS_RELWITHDEBINFO:STRING="-g" \
	-DOPENSYNC_UNITTESTS=OFF \
	-DCMAKE_SKIP_RPATH:BOOL=TRUE \
%if "%{?_lib}" == "lib64"
	-DLIB_SUFFIX=64 \
%endif
	-DCMAKE_VERBOSE_MAKEFILE:BOOL=TRUE

%build
cd build
make

%install
rm -rf $RPM_BUILD_ROOT
cd build
make install DESTDIR=$RPM_BUILD_ROOT
# remove bash completion script temporary
%__rm %{buildroot}/usr/etc/bash_completion.d/osynctool.sh

%clean
rm -rf $RPM_BUILD_ROOT

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%doc AUTHORS COPYING README
%{_bindir}/*

%changelog
* Tue Jun 28 2011 Chris Frey <cdfrey@foursquare.net> 0.39-1
- starting fresh rpm spec, based on Fedora's latest 0.22 spec

