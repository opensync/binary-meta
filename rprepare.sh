#!/bin/bash

if [ -z "$1" -o -z "$2" ] ; then
	echo "Usage:"
	echo
	echo "   rprepare.sh dirname specfile include_cmake"
	echo
	echo "dirname is the name of the directory under sources to prepare."
	echo "i.e. 'opensync-0.2x'  This does not include the sources/ path."
	echo
	echo "specfile is the full path of the spec file to copy into place"
	echo "under ~/rpmbuild/SPECS.   e.g. rpm/opensync-0.2x.spec"
	echo
	echo "include_cmake is an optional flag... if specified precisely"
	echo "then the sources/cmake-modules directory will be copied into"
	echo "the resulting tarball under the binary-meta-bits/ directory."
	echo
	echo "Example:"
	echo "     rprepare.sh opensync rpm/opensync-latest.spec include_cmake"
	echo
	exit 1
fi

set -e

DIRNAME="$1"
SPECFILE="$2"
OPTFLAG="$3"

# copy the spec file into place
cp "$SPECFILE" ~/rpmbuild/SPECS

# remove any existing source tarball from ~/rpmbuild
rm -f ~/rpmbuild/SOURCES/"$DIRNAME".tar.bz2

# copy the cmake-modules if necessary
if [ "$OPTFLAG" = "include_cmake" ] ; then
	echo "Copying cmake-modules into sources/$DIRNAME/binary-meta-bits"
	mkdir -p "sources/$DIRNAME/binary-meta-bits"
	cp -a sources/cmake-modules "sources/$DIRNAME/binary-meta-bits"
else
	echo "Skipping cmake-modules"
fi

# tar up the source directory and put the result in SOURCES
cd sources
tar --exclude=.git -cjf ~/rpmbuild/SOURCES/"$DIRNAME".tar.bz2 "$DIRNAME"

# cleanup the binary-meta-bits directory
rm -rf "$DIRNAME/binary-meta-bits"

