#!/bin/bash

#
# These functions contain the environment and other setup needed for
# any in-place builds.  The function names match the Makefile targets,
# except with - and . converted to _, and they are run only if the
# in-place dependency target file exists.
#
# That is, if deb-os-latest-build exists, then the opensync library was
# build from source, and therefore we need a special setup in order
# to build the plugin in-place as well.
#

function deb_barry_build() {
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$METADEBROOT2X/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export OPENSYNC22_CFLAGS="$(pkg-config --silence-errors --cflags opensync-1.0 | sed "s:-I:-I$METADEBROOT:")"
	export OPENSYNC40_CFLAGS="$(pkg-config --silence-errors --cflags libopensync1 | sed "s:-I:-I$METADEBROOT:")"
}

function deb_barry_0_2x_build() {
	# prepare the opensync-plugin directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p sources/barry/opensync-plugin/debian/libopensync0/DEBIAN
	cp debian/opensync-0.2x/debian/libopensync0/DEBIAN/shlibs \
		sources/barry/opensync-plugin/debian/libopensync0/DEBIAN

	# setup environment
	export PKG_CONFIG_PATH="$METADEBROOT2X/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export OPENSYNC2X_CFLAGS="$(pkg-config --silence-errors --cflags opensync-1.0 | sed "s:-I:-I$METADEBROOT2X:")"
	echo "OPENSYNC2X_CFLAGS set to: $OPENSYNC2X_CFLAGS"
	export LD_LIBRARY_PATH="$METADEBROOT2X/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT2X/usr/lib:$LIBRARY_PATH"
}

function deb_barry_0_4x_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p sources/barry/opensync-plugin-0.4x/debian/libopensync1/DEBIAN
	cp debian/opensync-latest/debian/libopensync1/DEBIAN/shlibs \
		sources/barry/opensync-plugin-0.4x/debian/libopensync1/DEBIAN

	# PKG_CONFIG_PATH is to find the opensync .pc file
	# LIBRARY_PATH is so gcc/ld can find -lopensync
	# LD_LIBRARY_PATH is so debhelper scripts can detect lib dependencies
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export OPENSYNC4X_CFLAGS="$(pkg-config --silence-errors --cflags libopensync1 | sed "s:-I:-I$METADEBROOT:")"
	echo "OPENSYNC4X_CFLAGS set to: $OPENSYNC4X_CFLAGS"
	export LD_LIBRARY_PATH="$METADEBROOT/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT/usr/lib:$LIBRARY_PATH"
}

function deb_evo2_0_2x_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/evolution2-0.2x/debian/libopensync0/DEBIAN
	cp debian/opensync-0.2x/debian/libopensync0/DEBIAN/shlibs \
		debian/evolution2-0.2x/debian/libopensync0/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT2X/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT2X/usr/include/opensync-1.0"
	export LD_LIBRARY_PATH="$METADEBROOT2X/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT2X/usr/lib:$LIBRARY_PATH"
}

function deb_evo2_latest_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/evolution2-latest/debian/libopensync1/DEBIAN
	cp debian/opensync-latest/debian/libopensync1/DEBIAN/shlibs \
		debian/evolution2-latest/debian/libopensync1/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT/usr/include/libopensync1"
	export LD_LIBRARY_PATH="$METADEBROOT/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT/usr/lib:$LIBRARY_PATH"
	export CMAKE_INCLUDE_PATH="$METADEBROOT/usr/share/libopensync1"
}

function deb_evo3_latest_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/evolution3-latest/debian/libopensync1/DEBIAN
	cp debian/opensync-latest/debian/libopensync1/DEBIAN/shlibs \
		debian/evolution3-latest/debian/libopensync1/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT/usr/include/libopensync1"
	export LD_LIBRARY_PATH="$METADEBROOT/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT/usr/lib:$LIBRARY_PATH"
	export CMAKE_INCLUDE_PATH="$METADEBROOT/usr/share/libopensync1"
}

function deb_fs_0_2x_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/file-sync-0.2x/debian/libopensync0/DEBIAN
	cp debian/opensync-0.2x/debian/libopensync0/DEBIAN/shlibs \
		debian/file-sync-0.2x/debian/libopensync0/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT2X/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT2X/usr/include/opensync-1.0"
	export LD_LIBRARY_PATH="$METADEBROOT2X/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT2X/usr/lib:$LIBRARY_PATH"
}

function deb_fs_latest_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/file-sync-latest/debian/libopensync1/DEBIAN
	cp debian/opensync-latest/debian/libopensync1/DEBIAN/shlibs \
		debian/file-sync-latest/debian/libopensync1/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT/usr/include/libopensync1"
	export LD_LIBRARY_PATH="$METADEBROOT/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT/usr/lib:$LIBRARY_PATH"
	export CMAKE_INCLUDE_PATH="$METADEBROOT/usr/share/libopensync1"
}

function deb_python_latest_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/python-module-latest/debian/libopensync1/DEBIAN
	cp debian/opensync-latest/debian/libopensync1/DEBIAN/shlibs \
		debian/python-module-latest/debian/libopensync1/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT/usr/include/libopensync1"
	export LD_LIBRARY_PATH="$METADEBROOT/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT/usr/lib:$LIBRARY_PATH"
	export CMAKE_INCLUDE_PATH="$METADEBROOT/usr/share/libopensync1"
}

function deb_google_0_2x_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/google-calendar-0.2x/debian/libopensync0/DEBIAN
	cp debian/opensync-0.2x/debian/libopensync0/DEBIAN/shlibs \
		debian/google-calendar-0.2x/debian/libopensync0/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT2X/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT2X/usr/include/opensync-1.0"
	export LD_LIBRARY_PATH="$METADEBROOT2X/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT2X/usr/lib:$LIBRARY_PATH"
}

function deb_google_latest_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/google-calendar-latest/debian/libopensync1/DEBIAN
	cp debian/opensync-latest/debian/libopensync1/DEBIAN/shlibs \
		debian/google-calendar-latest/debian/libopensync1/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT/usr/include/libopensync1"
	export LD_LIBRARY_PATH="$METADEBROOT/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT/usr/lib:$LIBRARY_PATH"
	export CMAKE_INCLUDE_PATH="$METADEBROOT/usr/share/libopensync1"
}

function deb_akonadi_latest_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/akonadi-sync-latest/debian/libopensync1/DEBIAN
	cp debian/opensync-latest/debian/libopensync1/DEBIAN/shlibs \
		debian/akonadi-sync-latest/debian/libopensync1/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT/usr/include/libopensync1"
	export CPLUS_INCLUDE_PATH="$METADEBROOT/usr/include/libopensync1"
	export LD_LIBRARY_PATH="$METADEBROOT/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT/usr/lib:$LIBRARY_PATH"
	export CMAKE_INCLUDE_PATH="$METADEBROOT/usr/share/libopensync1"
}

function deb_kdepim_0_2x_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/kdepim-0.2x/debian/libopensync0/DEBIAN
	cp debian/opensync-0.2x/debian/libopensync0/DEBIAN/shlibs \
		debian/kdepim-0.2x/debian/libopensync0/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT2X/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT2X/usr/include/opensync-1.0"
	export CPLUS_INCLUDE_PATH="$METADEBROOT2X/usr/include/opensync-1.0"
	export LD_LIBRARY_PATH="$METADEBROOT2X/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT2X/usr/lib:$LIBRARY_PATH"
}

function deb_kdepim_latest_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/kdepim-latest/debian/libopensync1/DEBIAN
	cp debian/opensync-latest/debian/libopensync1/DEBIAN/shlibs \
		debian/kdepim-latest/debian/libopensync1/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT/usr/include/libopensync1"
	export CPLUS_INCLUDE_PATH="$METADEBROOT2X/usr/include/opensync-1.0"
	export LD_LIBRARY_PATH="$METADEBROOT/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT/usr/lib:$LIBRARY_PATH"
	export CMAKE_INCLUDE_PATH="$METADEBROOT/usr/share/libopensync1"
}

function deb_vformat_latest_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/vformat-latest/debian/libopensync1/DEBIAN
	cp debian/opensync-latest/debian/libopensync1/DEBIAN/shlibs \
		debian/vformat-latest/debian/libopensync1/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT/usr/include/libopensync1"
	export LD_LIBRARY_PATH="$METADEBROOT/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT/usr/lib:$LIBRARY_PATH"
	export CMAKE_INCLUDE_PATH="$METADEBROOT/usr/share/libopensync1"
}

function deb_xmlformat_latest_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/xmlformat-latest/debian/libopensync1/DEBIAN
	cp debian/opensync-latest/debian/libopensync1/DEBIAN/shlibs \
		debian/xmlformat-latest/debian/libopensync1/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT/usr/include/libopensync1"
	export LD_LIBRARY_PATH="$METADEBROOT/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT/usr/lib:$LIBRARY_PATH"
	export CMAKE_INCLUDE_PATH="$METADEBROOT/usr/share/libopensync1"
}

function deb_xslt_latest_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/xsltformat-latest/debian/libopensync1/DEBIAN
	cp debian/opensync-latest/debian/libopensync1/DEBIAN/shlibs \
		debian/xsltformat-latest/debian/libopensync1/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT/usr/include/libopensync1"
	export LD_LIBRARY_PATH="$METADEBROOT/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT/usr/lib:$LIBRARY_PATH"
	export CMAKE_INCLUDE_PATH="$METADEBROOT/usr/share/libopensync1"
}

function deb_osynctool_latest_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/osynctool-latest/debian/libopensync1/DEBIAN
	cp debian/opensync-latest/debian/libopensync1/DEBIAN/shlibs \
		debian/osynctool-latest/debian/libopensync1/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT/usr/include/libopensync1"
	export LD_LIBRARY_PATH="$METADEBROOT/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT/usr/lib:$LIBRARY_PATH"
	export CMAKE_INCLUDE_PATH="$METADEBROOT/usr/share/libopensync1"
}

function deb_msynctool_0_2x_build() {
	# prepare the opensync-plugin-0.4x directory to contain the
	# opensync shlibs file, for the debian lib dependency scripts
	mkdir -p debian/msynctool-0.2x/debian/libopensync0/DEBIAN
	cp debian/opensync-0.2x/debian/libopensync0/DEBIAN/shlibs \
		debian/msynctool-0.2x/debian/libopensync0/DEBIAN

	# build
	export PKG_CONFIG_PATH="$METADEBROOT2X/usr/lib/pkgconfig:$PKG_CONFIG_PATH"
	export C_INCLUDE_PATH="$METADEBROOT2X/usr/include/opensync-1.0"
	export LD_LIBRARY_PATH="$METADEBROOT2X/usr/lib:$LD_LIBRARY_PATH"
	export LIBRARY_PATH="$METADEBROOT2X/usr/lib:$LIBRARY_PATH"
}


##############################################################################
# RPM settings
##############################################################################

function rpm_barry_0_2x_build() {
	export ADD_TO_PKG_CONFIG_PATH="$METARPMROOT2X/usr/lib$LIB_SUFFIX/pkgconfig"
	export LIBRARY_PATH="$METARPMROOT2X/usr/lib$LIB_SUFFIX:$LIBRARY_PATH"
	export LD_LIBRARY_PATH="$METARPMROOT2X/usr/lib$LIB_SUFFIX:$LD_LIBRARY_PATH"

	# override pkgconfig path for commands below (rpmbuild may not
	# recognize this though)
	export PKG_CONFIG_PATH="$ADD_TO_PKG_CONFIG_PATH:$PKG_CONFIG_PATH"

	# override pkgconfig flags to desktop's configure
	export OPENSYNC22_CFLAGS="$(pkg-config --silence-errors --cflags opensync-1.0 | sed "s:-I:-I$METARPMROOT:")"
#	export OPENSYNC40_CFLAGS="$(pkg-config --silence-errors --cflags libopensync1 | sed "s:-I:-I$METARPMROOT:")"

	# override pkgconfig flags to opensync-plugin's configure
	export OPENSYNC2X_CFLAGS="$(pkg-config --silence-errors --cflags opensync-1.0 | sed "s:-I:-I$METARPMROOT2X:")"
	echo "OPENSYNC2X_CFLAGS set to: $OPENSYNC2X_CFLAGS"
}

function rpm_barry_0_4x_build() {
	export ADD_TO_PKG_CONFIG_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX/pkgconfig"
	export LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LIBRARY_PATH"
	export LD_LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LD_LIBRARY_PATH"

	# override pkgconfig path for commands below (rpmbuild may not
	# recognize this though)
	export PKG_CONFIG_PATH="$ADD_TO_PKG_CONFIG_PATH:$PKG_CONFIG_PATH"

	# override pkgconfig flags to desktop's configure
#	export OPENSYNC22_CFLAGS="$(pkg-config --silence-errors --cflags opensync-1.0 | sed "s:-I:-I$METARPMROOT:")"
	export OPENSYNC40_CFLAGS="$(pkg-config --silence-errors --cflags libopensync1 | sed "s:-I:-I$METARPMROOT:")"

	# override pkgconfig flags to opensync-plugin's configure
	export OPENSYNC4X_CFLAGS="$(pkg-config --silence-errors --cflags libopensync1 | sed "s:-I:-I$METARPMROOT:")"
	echo "OPENSYNC4X_CFLAGS set to: $OPENSYNC4X_CFLAGS"
}

function rpm_barry_both_build() {
	export OSYNC4X_PKG_CONFIG_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX/pkgconfig"
	export OSYNC2X_PKG_CONFIG_PATH="$METARPMROOT2X/usr/lib$LIB_SUFFIX/pkgconfig"
	export OSYNCBOTH_PKG_CONFIG_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX/pkgconfig:$METARPMROOT2X/usr/lib$LIB_SUFFIX/pkgconfig"

	# override pkgconfig flags to desktop's configure
	# The desktop compile does not use _LIBS overrides, since it
	# loads libraries via dlopen().
	export OPENSYNC22_CFLAGS="$(PKG_CONFIG_PATH="$OSYNC2X_PKG_CONFIG_PATH" pkg-config --silence-errors --cflags opensync-1.0 | sed "s:-I:-I$METARPMROOT:")"
	export OPENSYNC40_CFLAGS="$(PKG_CONFIG_PATH="$OSYNC4X_PKG_CONFIG_PATH" pkg-config --silence-errors --cflags libopensync1 | sed "s:-I:-I$METARPMROOT:")"

	# override pkgconfig flags to opensync-plugin's configure
	export OPENSYNC2X_CFLAGS="$(PKG_CONFIG_PATH="$OSYNC2X_PKG_CONFIG_PATH" pkg-config --silence-errors --cflags opensync-1.0 | sed "s:-I:-I$METARPMROOT2X:")"
	echo "OPENSYNC2X_CFLAGS set to: $OPENSYNC2X_CFLAGS"
	export OPENSYNC2X_LIBS="$(PKG_CONFIG_PATH="$OSYNC2X_PKG_CONFIG_PATH" pkg-config --silence-errors --libs opensync-1.0 | sed "s:^:-L$METARPMROOT2X/usr/lib$LIB_SUFFIX :")"
	echo "OPENSYNC2X_LIBS set to: $OPENSYNC2X_LIBS"

	# override pkgconfig flags to opensync-plugin's configure
	export OPENSYNC4X_CFLAGS="$(PKG_CONFIG_PATH="$OSYNC4X_PKG_CONFIG_PATH" pkg-config --silence-errors --cflags libopensync1 | sed "s:-I:-I$METARPMROOT:")"
	echo "OPENSYNC4X_CFLAGS set to: $OPENSYNC4X_CFLAGS"
	export OPENSYNC4X_LIBS="$(PKG_CONFIG_PATH="$OSYNC4X_PKG_CONFIG_PATH" pkg-config --silence-errors --libs libopensync1 | sed "s:^:-L$METARPMROOT/usr/lib$LIB_SUFFIX :")"
	echo "OPENSYNC4X_LIBS set to: $OPENSYNC4X_LIBS"
}

function rpm_xmlformat_0_4x_build() {
	export ADD_TO_PKG_CONFIG_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX/pkgconfig"
	export LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LIBRARY_PATH"
	export LD_LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LD_LIBRARY_PATH"
	export C_INCLUDE_PATH="$METARPMROOT/usr/include/libopensync1"
	export CMAKE_INCLUDE_PATH="$METARPMROOT/usr/share/libopensync1"
}

function rpm_vformat_0_4x_build() {
	export ADD_TO_PKG_CONFIG_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX/pkgconfig"
	export LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LIBRARY_PATH"
	export LD_LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LD_LIBRARY_PATH"
	export C_INCLUDE_PATH="$METARPMROOT/usr/include/libopensync1"
	export CMAKE_INCLUDE_PATH="$METARPMROOT/usr/share/libopensync1"
}

function rpm_osynctool_0_4x_build() {
	export ADD_TO_PKG_CONFIG_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX/pkgconfig"
	export LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LIBRARY_PATH"
	export LD_LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LD_LIBRARY_PATH"
	export C_INCLUDE_PATH="$METARPMROOT/usr/include/libopensync1"
	export CMAKE_INCLUDE_PATH="$METARPMROOT/usr/share/libopensync1"
}

function rpm_msynctool_0_2x_build() {
	export ADD_TO_PKG_CONFIG_PATH="$METARPMROOT2X/usr/lib$LIB_SUFFIX/pkgconfig"
	# override pkgconfig path for commands below (rpmbuild may not
	# recognize this though)
	export PKG_CONFIG_PATH="$ADD_TO_PKG_CONFIG_PATH:$PKG_CONFIG_PATH"

	export LIBRARY_PATH="$METARPMROOT2X/usr/lib$LIB_SUFFIX:$LIBRARY_PATH"
	export LD_LIBRARY_PATH="$METARPMROOT2X/usr/lib$LIB_SUFFIX:$LD_LIBRARY_PATH"
	export C_INCLUDE_PATH="$METARPMROOT2X/usr/include/opensync-1.0"
}

function rpm_evo2_0_2x_build() {
	export ADD_TO_PKG_CONFIG_PATH="$METARPMROOT2X/usr/lib$LIB_SUFFIX/pkgconfig"
	# override pkgconfig path for commands below (rpmbuild may not
	# recognize this though)
	export PKG_CONFIG_PATH="$ADD_TO_PKG_CONFIG_PATH:$PKG_CONFIG_PATH"

	export LIBRARY_PATH="$METARPMROOT2X/usr/lib$LIB_SUFFIX:$LIBRARY_PATH"
	export LD_LIBRARY_PATH="$METARPMROOT2X/usr/lib$LIB_SUFFIX:$LD_LIBRARY_PATH"
	export C_INCLUDE_PATH="$METARPMROOT2X/usr/include/opensync-1.0"
}

function rpm_evo2_0_4x_build() {
	export ADD_TO_PKG_CONFIG_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX/pkgconfig"
	export LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LIBRARY_PATH"
	export LD_LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LD_LIBRARY_PATH"
	export C_INCLUDE_PATH="$METARPMROOT/usr/include/libopensync1"
	export CMAKE_INCLUDE_PATH="$METARPMROOT/usr/share/libopensync1"
}

function rpm_evo3_0_4x_build() {
	export ADD_TO_PKG_CONFIG_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX/pkgconfig"
	export LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LIBRARY_PATH"
	export LD_LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LD_LIBRARY_PATH"
	export C_INCLUDE_PATH="$METARPMROOT/usr/include/libopensync1"
	export CMAKE_INCLUDE_PATH="$METARPMROOT/usr/share/libopensync1"
}

function rpm_filesync_0_4x_build() {
	export ADD_TO_PKG_CONFIG_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX/pkgconfig"
	export LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LIBRARY_PATH"
	export LD_LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LD_LIBRARY_PATH"
	export C_INCLUDE_PATH="$METARPMROOT/usr/include/libopensync1"
	export CMAKE_INCLUDE_PATH="$METARPMROOT/usr/share/libopensync1"
}

function rpm_pythonmodule_0_4x_build() {
	export ADD_TO_PKG_CONFIG_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX/pkgconfig"
	export LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LIBRARY_PATH"
	export LD_LIBRARY_PATH="$METARPMROOT/usr/lib$LIB_SUFFIX:$LD_LIBRARY_PATH"
	export C_INCLUDE_PATH="$METARPMROOT/usr/include/libopensync1"
	export CMAKE_INCLUDE_PATH="$METARPMROOT/usr/share/libopensync1"
}


# if first argument exists as a file, run the second argument as a function
if [ -f "$1" ] ; then
	$(echo "$2" | tr '.-' '__')
fi

