#!/bin/bash

#
# This script is meant to be run from inside one of the source trees,
# in case you wish to work on just that tree, instead of the the whole
# thing.  This script will create a build directory and run cmake for
# you, with default flags, using the sources/cmake-modules directory
# as needed.
#

pushd "$(dirname "$0")"
SOURCESPATH="$(pwd)"
popd

export PKG_CONFIG_PATH="$SOURCESPATH/rootdir/lib/pkgconfig:$PKG_CONFIG_PATH"

mkdir build
cd build
cmake -Wdev \
	-DCMAKE_MODULE_PATH="$SOURCESPATH/cmake-modules" \
	-DCMAKE_INSTALL_PREFIX="$SOURCESPATH/rootdir" \
	-DCMAKE_ETC_INSTALL_PREFIX="$SOURCESPATH/rootdir" \
	-DCMAKE_C_FLAGS:STRING="-Wall -Werror -O0 -fno-strict-aliasing" \
	-DCMAKE_CXX_FLAGS:STRING="-Wall -Werror -O0 -fno-strict-aliasing" \
	-DADDITIONAL_SWIG_CFLAGS:STRING="-Wno-error" \
	-DCMAKE_C_FLAGS_RELWITHDEBINFO:STRING="-g" \
	-DCMAKE_VERBOSE_MAKEFILE:BOOL=TRUE \
	-DCMAKE_SKIP_RPATH:BOOL=TRUE \
	..

echo "You can now run: cd build ; make ; make install"

#	-DCMAKE_C_FLAGS="-std\\=c99 -Wall -Werror -pedantic -O0" \
#	-DCMAKE_C_COMPILER:STRING=/usr/local/bin/ccache_gcc \
#	-DCMAKE_CXX_COMPILER:STRING=/usr/local/bin/ccache_g++ \

