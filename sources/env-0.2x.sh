#!/bin/bash

#
# Sets environment for using the opensync src build installed in rootdir-0.2x.
# Spawns a new shell to hold the variables, and to make it easy to get
# rid of them when finished.
#

pushd "$(dirname "$0")" > /dev/null
SOURCESPATH="$(pwd)"
popd > /dev/null

# For building...
export PKG_CONFIG_PATH="$SOURCESPATH/rootdir-0.2x/lib/pkgconfig:$PKG_CONFIG_PATH"

# For running...
export LD_LIBRARY_PATH="$SOURCESPATH/rootdir-0.2x/lib:$LD_LIBRARY_PATH"

# For command line...
export PATH="$SOURCESPATH/rootdir/sbin:$SOURCESPATH/rootdir/bin:$PATH"

# For debugging...
mkdir -p "$SOURCESPATH/rootdir-0.2x/trace_logs"
export OSYNC_TRACE="$SOURCESPATH/rootdir-0.2x/trace_logs"

# Let user know what's what
echo "New environment settings:"
echo "========================="
set | egrep '^(PKG_CONFIG_PATH|LD_LIBRARY_PATH|OSYNC_TRACE)'
echo

# Starting new shell to work in, with these new settings
echo "Spawning new shell.  Exit shell to remove environment overrides."
bash

