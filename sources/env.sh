#!/bin/bash

#
# Sets environment for using the opensync src build installed in rootdir.
# Spawns a new shell to hold the variables, and to make it easy to get
# rid of them when finished.
#

pushd "$(dirname "$0")" > /dev/null
SOURCESPATH="$(pwd)"
popd > /dev/null

# For building...
export PKG_CONFIG_PATH="$SOURCESPATH/rootdir/lib/pkgconfig:$PKG_CONFIG_PATH"

# For running...
export LD_LIBRARY_PATH="$SOURCESPATH/rootdir/lib:$LD_LIBRARY_PATH"

# For command line...
export PATH="$SOURCESPATH/rootdir/sbin:$SOURCESPATH/rootdir/bin:$PATH"

# For debugging...
mkdir -p "$SOURCESPATH/rootdir/trace_logs"
export OSYNC_TRACE="$SOURCESPATH/rootdir/trace_logs"
export OSYNC_NOPRIVACY=1

# Let user know what's what
echo "New environment settings:"
echo "========================="
set | egrep '^(PKG_CONFIG_PATH|LD_LIBRARY_PATH|OSYNC_TRACE|OSYNC_NOPRIVACY)'
echo

# Starting new shell to work in, with these new settings
echo "Spawning new shell.  Exit shell to remove environment overrides."
bash

